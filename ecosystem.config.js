module.exports = {
    apps : [{
      name: "VRP_Back",
      script: "./dist/bin/www.js",
      env: {
        NODE_ENV: "development",
      },
      env_production: {
        NODE_ENV: "production",
      }
    }]
  }
