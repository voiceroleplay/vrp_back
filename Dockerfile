FROM node:alpine
ENV SWAGGER=vps1.webcoastagency.com
ENV NODE_ENV=production
# Create API Directory
RUN rm -rf /usr/src/app
RUN mkdir -p /usr/src/app/vrp-back
WORKDIR /usr/src/app/vrp-back

COPY package.json ./
RUN npm install --silent

COPY . .
EXPOSE 3000
CMD ["npm", "start"]