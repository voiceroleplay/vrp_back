var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var app = express();
require('dotenv').config()
const Sentry = require('@sentry/node');
const expressSwagger = require('express-swagger-generator')(app);

var index = require('./routes/index');
var aventure = require('./routes/aventure');
var personnage = require('./routes/personnage');
var story = require('./routes/story');
var account = require('./routes/account');
var monster = require('./routes/monster');
var spell = require('./routes/spell');
var feat = require('./routes/feat');
var classe = require('./routes/classe');
var race = require('./routes/race');
var vocalReconize = require('./routes/vocalReconize')

var mongoose = require('mongoose');
const { MONGODB_URL } = require('./config/config')

var auth = require('./routes/auth/auth');

Sentry.init({ dsn: 'https://aed7ecf902bb47948c65d1d59e02df16@sentry.io/5175739', debug: true, environement: 'local' });

if (process.env.NODE_ENV == 'production') {
  mongoose.connect(MONGODB_URL, {useNewUrlParser: true, useUnifiedTopology: true})
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
    });
}

// Configuration SWAGGER
var configSwagger = require('./config/configSwagger');
expressSwagger(configSwagger);

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use(Sentry.Handlers.requestHandler());


app.use('*', auth.checkAuth);
app.use('/', index);
app.use('/aventure', aventure);
app.use('/personnage', personnage);
app.use('/story', story);
app.use('/account', account);
app.use('/monster', monster);
app.use('/spell', spell);
app.use('/feat', feat);
app.use('/classe', classe);
app.use('/race', race);
app.use('/vocalReconize', vocalReconize);

app.use(Sentry.Handlers.errorHandler());

app.use(function onError(err, req, res, next) {
  // The error id is attached to `res.sentry` to be returned
  // and optionally displayed to the user for support.
  res.statusCode = 500;
  res.end(res.sentry, "\n");
});

module.exports = app;
