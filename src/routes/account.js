var express = require('express');
var router = express.Router();
var account = require('../request/account');

/**
 * This function comment is parsed by doctrine
 * @route GET /account
 * @group Account
 * @param { string } _id.query.require
 * @param { string } lastname.query.require
 * @param { string } firstname.query.require
 * @param { string } username.query.require
 * @returns { Array }  200 - Array of Account 
 * @returns {Error}  default - Unexpected error
 */
router.get('', function(req, res, next) {
  try {
    account.find(req.query, res);
  } catch (error) {
    res.json(error);
  }
});

/**
 * This function comment is parsed by doctrine
 * @route POST /account
 * @group Account
 * @param { object } account.body.require - eg: Object type account (_id require)
 * @consumes application/json
 * @returns  204 - Account created 
 * @returns {Error}  default - Unexpected error
 */
router.post('', function(req, res, next) {
  account.create(req.body, res);
});

/**
 * This function comment is parsed by doctrine
 * @route PUT /account
 * @group Account
 * @param { object } account.body.require - eg: Object type account
 * @consumes application/json
 * @returns  204 - Account modified 
 * @returns {Error}  default - Unexpected error
 */
router.put('', function(req, res) {
  try {
    account.save(req.body, res);
  } catch (error) {
    res.json(error);
  }
});

/**
 * This function comment is parsed by doctrine
 * @route DELETE /account
 * @group Account
 * @param { string } _id.query.require
 * @returns  204 - Account deleted 
 * @returns {Error}  default - Unexpected error
 */
router.delete('', function(req, res, next) {
  account.delete(req.query, res)
})


module.exports = router;