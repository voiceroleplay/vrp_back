var express = require('express');
var router = express.Router();
var aventure = require('../request/aventure');

/**
 * This function comment is parsed by doctrine
 * @route GET /aventure
 * @group Aventure
 * @param { string } name.query.require
 * @param { string } _id.query.require
 * @param { string } gm.query.require
 * @param { string } room.query.require
 * @param { string } players.query.require
 * @returns { Array }  200 - Array of Aventure 
 * @returns {Error}  default - Unexpected error
 */
router.get('', function(req, res) {
  try {
    aventure.find(req.query, res);
  } catch (error) {
    res.json(error);
  }
});


/**
 * This function comment is parsed by doctrine
 * @route PUT /aventure
 * @group Aventure
 * @param { object } aventure.body.require - eg: Object type Aventure (_id require)
 * @consumes application/json
 * @returns  204 - Aventure modified 
 * @returns {Error}  default - Unexpected error
 */
router.put('', function(req, res) {
  try {
    aventure.save(req.body, res);
  } catch (error) {
    res.json(error);
  }
});
  
/**
 * This function comment is parsed by doctrine
 * @route POST /aventure
 * @group Aventure
 * @param { object } aventure.body.require - eg: Object type aventure
 * @consumes application/json
 * @returns  204 - Aventure created
 * @returns {Error}  default - Unexpected error
 */
router.post('', function(req, res) {
  try {
    aventure.create(req.body, res);
  } catch (error) {
    res.json(error);
  }
});

/**
 * This function comment is parsed by doctrine
 * @route DELETE /aventure
 * @group Aventure
 * @param { string } _id.query.require
 * @returns  204 - Aventure deleted 
 * @returns {Error}  default - Unexpected error
 */
router.delete('', function(req, res) {
  try {
    aventure.delete(req.query, res)
  } catch (error) {
    res.json(error);
  }
})

module.exports = router;