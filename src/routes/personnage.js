var express = require('express');
var router = express.Router();
var personnage = require('../request/personnage');

/**
 * This function comment is parsed by doctrine
 * @route GET /personnage
 * @group Personnage
 * @param { string } username.query.require
 * @param { string } _id.query.require
 * @returns { Array }  200 - Array of Personnage 
 * @returns {Error}  default - Unexpected error
 */
router.get('', function(req, res) {
  try {
    personnage.find(req.query, res);
  } catch (error) {
    res.json(error);
  }
});

/**
 * This function comment is parsed by doctrine
 * @route PUT /personnage
 * @group Personnage
 * @param { object } personnage.body.require - eg: Object type personnage (_id require)
 * @consumes application/json
 * @returns  204 - Personnage modified 
 * @returns {Error}  default - Unexpected error
 */
router.put('', function(req, res) {
  try {
    personnage.save(req.body, res);
  } catch (error) {
    res.json(error);
  }
});
  
/**
 * This function comment is parsed by doctrine
 * @route POST /personnage
 * @group Personnage
 * @param { object } personnage.body.require - eg: Object type personnage
 * @consumes application/json
 * @returns  204 - Personnage created 
 * @returns {Error}  default - Unexpected error
 */
router.post('', function(req, res) {
  try {
    personnage.create(req.body, res);
  } catch (error) {
    res.json(error);
  }
});

/**
 * This function comment is parsed by doctrine
 * @route DELETE /personnage
 * @group Personnage
 * @param { string } _id.query.require
 * @returns  204 - Personnage deleted 
 * @returns {Error}  default - Unexpected error
 */
router.delete('', function(req, res) {
  try {
    personnage.delete(req.query, res)
  } catch (error) {
    res.json(error);
  }
})

module.exports = router;