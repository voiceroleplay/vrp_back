var admin = require("firebase-admin");
var serviceAccount = require("../../config/fbServiceAccountKey.js");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://voiceroleplay-b3ae9.firebaseio.com"
});
  
var test = {};
test.checkAuth = function (req, res, next) {
  if (req.headers.authtoken) {
    admin.auth().verifyIdToken(req.headers.authtoken)
      .then(() => {
        next()
    }).catch(() => {
        res.status(403).send('Unauthorized')
      });
  } else if (req.originalUrl == '/authToken') {
    next();
  } else if (req.originalUrl == '/account' && req.method == 'POST') {
    next();
  } else {
    res.status(403).send('Unauthorized')
  }
}

module.exports = test;