var express = require('express');
var router = express.Router();
var feat = require('../request/feat');

/**
 * This function comment is parsed by doctrine
 * @route GET /feat
 * @group Feat
 * @param { string } Name.query.require
 * @param { string } _id.query.require
 * @returns { Array }  200 - Array of Feat 
 * @returns {Error}  default - Unexpected error
 */
router.get('', function(req, res, next) {
  try {
    feat.find(req.query, res);
  } catch (error) {
    res.json(error);
  }
});

module.exports = router;