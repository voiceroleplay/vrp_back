var express = require('express');
var router = express.Router();
var dices = require('../request/vocalReconize');

/**
 * This function comment is parsed by doctrine
 * @route GET /vocalReconize
 * @group VocalReconize
 * @param { string } name.query.require
 * @param { string } _id.query.require
 * @returns { Array }  200 - Array of Story 
 * @returns {Error}  default - Unexpected error
 */
router.get('', function(req, res, next) {
    dices.getDices(req.query.val, res);
  });

module.exports = router;