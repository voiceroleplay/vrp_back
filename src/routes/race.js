var express = require('express');
var router = express.Router();
var race = require('../request/race');

/**
 * This function comment is parsed by doctrine
 * @route GET /race
 * @group Race
 * @param { string } name.query.require
 * @param { string } Id.query.require
 * @returns { Array }  200 - Array of Race 
 * @returns {Error}  default - Unexpected error
 */
router.get('', function(req, res, next) {
  try {
    race.find(req.query, res);
  } catch (error) {
    res.json(error);
  }
});

module.exports = router;