var express = require('express');
var router = express.Router();
var admin = require("firebase-admin");
var rp = require('request-promise')

/**
 * This function comment is parsed by doctrine
 * @route GET /
 * @group  Index
 * @returns Hello World! 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */
router.get('/', function(req, res, next) {
  res.json({
    message: 'Hello World!'
  })
});

router.get('/debug', function(req, res, next) {
  res.status(500)
    .send('My debug route for sentry')
});

/**
 * This function comment is parsed by doctrine
 * @route GET /authToken
 * @group Index
 * @returns token for test
 * @returns {Error}  default - Unexpected error
 */
router.get('/authToken', async function(req, res, next) {
  const uid = 'test-uid'
  customToken = await admin.auth().createCustomToken(uid);

  const token = await rp({
    url: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyCustomToken?key=AIzaSyDGtuzBKaNR2ouVRhdW1Nc7PW56jOoK32g',
    method: 'POST',
    body: {
        token: customToken,
        returnSecureToken: true,
    },
    json: true
  });

  idToken = token.idToken;

  res.json({
    token: idToken
  })
});

module.exports = router;
