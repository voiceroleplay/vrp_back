var express = require('express');
var router = express.Router();
var classe = require('../request/classe');

/**
 * This function comment is parsed by doctrine
 * @route GET /classe
 * @group Classe
 * @param { string } name.query.require
 * @param { string } Id.query.require
 * @returns { Array }  200 - Array of Classe 
 * @returns {Error}  default - Unexpected error
 */
router.get('', function(req, res, next) {
  try {
    classe.find(req.query, res);
  } catch (error) {
    res.json(error);
  }
});

module.exports = router;