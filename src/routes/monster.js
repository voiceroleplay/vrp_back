var express = require('express');
var router = express.Router();
var monster = require('../request/monster');

/**
 * This function comment is parsed by doctrine
 * @route GET /monster
 * @group Monster
 * @param { string } _name.query.require 
 * @param { string } _id.query.require
 * @param { string } _climate.query.require
 * @param { string } _environment.query.require
 * @param { string } _type.query.require
 * @returns { Array }  200 - Array of Monster 
 * @returns {Error}  default - Unexpected error
 */
router.get('', function(req, res, next) {
  try {
    monster.find(req.query, res);
  } catch (error) {
    res.json(error);
  }
});

module.exports = router;