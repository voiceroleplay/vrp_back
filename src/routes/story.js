var express = require('express');
var router = express.Router();
var story = require('../request/story');

/**
 * This function comment is parsed by doctrine
 * @route GET /story
 * @group Story
 * @param { string } name.query.require
 * @param { string } _id.query.require
 * @returns { Array }  200 - Array of Story 
 * @returns {Error}  default - Unexpected error
 */
router.get('', function(req, res, next) {
  try {
    story.find(req.query, res);
  } catch (error) {
    res.json(error);
  }
});

/**
 * This function comment is parsed by doctrine
 * @route POST /story
 * @group Story
 * @param { object } story.body.require - eg: Object type Story
 * @consumes application/json
 * @returns  204 - Story created 
 * @returns {Error}  default - Unexpected error
 */
router.post('', function(req, res, next) {
  story.create(req.body, res);
});

/**
 * This function comment is parsed by doctrine
 * @route PUT /story
 * @group Story
 * @param { object } story.body.require - eg: Object type Story (_id require)
 * @consumes application/json
 * @returns  204 - Story modified 
 * @returns {Error}  default - Unexpected error
 */
router.put('', function(req, res) {
  try {
    story.save(req.body, res);
  } catch (error) {
    res.json(error);
  }
});

/**
 * This function comment is parsed by doctrine
 * @route DELETE /story
 * @group Story
 * @param { string } _id.query.require
 * @returns  204 - Story deleted 
 * @returns {Error}  default - Unexpected error
 */
router.delete('', function(req, res, next) {
  story.delete(req.query, res)
});

module.exports = router;