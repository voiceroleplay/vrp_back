var express = require('express');
var router = express.Router();
var spell = require('../request/spell');

/**
 * This function comment is parsed by doctrine
 * @route GET /spell
 * @group Spell
 * @param { string } Name.query.require 
 * @param { string } _id.query.require
 * @param { string } School.query.require
 * @returns { Array }  200 - Array of Spell 
 * @returns {Error}  default - Unexpected error
 */
router.get('', function(req, res, next) {
  try {
    spell.find(req.query, res);
  } catch (error) {
    res.json(error);
  }
});

module.exports = router;