var { Account } = require('../model/account');

exports.find = async (request, res) => {
    const account = await Account.find(request);
    res.json(account);
}

exports.save = async (request, res) => {
    const account = await Account.findOne({_id : request._id});
    if(account) {
        await Account.updateOne({_id : request._id}, request);
        res.json("Your Personnage has been saved");
    } else {
        res.json("Your Personnage has not been saved");
    }
}

exports.create = async (request, res) => {
    const account = await Account.findOne({username : request.username});
    if(!account) {
        await Account.create(request);
        res.json("Your account has been created");
    }
    else {
        res.json("Error: This username is already used");
    }
}

exports.delete = async (request, res) => {
    await Account.deleteOne(request);
    res.json("Your account has been deleted");
}