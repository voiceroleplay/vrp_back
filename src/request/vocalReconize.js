exports.getDices = async (request, res) => {
    var theString = request;
    
    var regexp = /lance\s(dix-sept|dix\ssept|17|dix-huit|dix\shuit|18|dix-neuf|dix\sneuf|19|dix|10|onze|11|douze|12|treize|13|quatorze|14|quinze|15|seize|16|20|vingt|vins|vin|un|1|deux|2|de|trois|3|quatre|4|cinq|5|six|6|sept|7|huit|8|neuf|9)\s(des|dès|de|dé|dés)\s(4|quatre|6|six|8|huit|100|cent|cents|sans|sang|10|dix|12|douze|20|vingt|vins|vin)/igm;
    if(regexp.test(theString)){
        var regexpDes = /(des|dès|de|dé|dés)\s(4|quatre|6|six|8|huit|100|cent|cents|sans|sang|10|dix|12|douze|20|vingt|vins|vin)/igm;
        var regexpLance = /lance\s(dix-sept|dix\ssept|17|dix-huit|dix\shuit|18|dix-neuf|dix\sneuf|19|dix|10|onze|11|douze|12|treize|13|quatorze|14|quinze|15|seize|16|20|vingt|vins|vin|un|1|deux|2|de|trois|3|quatre|4|cinq|5|six|6|sept|7|huit|8|neuf|9)/igm;
        var tagDes = theString.match(regexpDes);
        var tagLance = theString.match(regexpLance);
        var tabDes = [];
        var nbDes = 0;
        var valDes = 0;
        var total = 0;

        if((/4|quatre/igm).test(tagDes[0])) {
            valDes = 4;
        } else if((/6|six/igm).test(tagDes[0])) {
            valDes = 6;
        } else if((/8|huit/igm).test(tagDes[0])) {
            valDes = 8;
        } else if((/12|douze/igm).test(tagDes[0])) {
            valDes = 12;
        } else if((/20|vingt|vins|vin/igm).test(tagDes[0])) {
            valDes = 20;
        } else if((/100|cents|cent|sang/igm).test(tagDes[0])) {
            valDes = 100;
        } else {
            valDes = 10;
        }

        if((/17|dix-sept|dix\ssept/igm).test(tagLance[0])) {
            nbDes = 17;
        } else if((/18|dix-huit|dix\shuit/igm).test(tagLance[0])) {
            nbDes = 18;
        } else if((/19|dix-neuf|dix\sneuf/igm).test(tagLance[0])) {
            nbDes = 19;
        } else if((/10|dix/igm).test(tagLance[0])) {
            nbDes = 10;
        } else if((/11|onze/igm).test(tagLance[0])) {
            nbDes = 11;
        } else if((/12|douze/igm).test(tagLance[0])) {
            nbDes = 12;
        } else if((/13|treize/igm).test(tagLance[0])) {
            nbDes = 13;
        } else if((/14|quatorze/igm).test(tagLance[0])) {
            nbDes = 14;
        } else if((/15|quinze/igm).test(tagLance[0])) {
            nbDes = 15;
        } else if((/16|seize/igm).test(tagLance[0])) {
            nbDes = 16;
        } else if((/20|vingt|vins|vin/igm).test(tagLance[0])) {
            nbDes = 20;
        } else if((/2|deux|de/igm).test(tagLance[0])) {
            nbDes = 2;
        } else if((/3|trois/igm).test(tagLance[0])) {
            nbDes = 3;
        } else if((/4|quatre/igm).test(tagLance[0])) {
            nbDes = 4;
        } else if((/5|cinq/igm).test(tagLance[0])) {
            nbDes = 5;
        } else if((/6|six/igm).test(tagLance[0])) {
            nbDes = 6;
        } else if((/7|sept/igm).test(tagLance[0])) {
            nbDes = 7;
        } else if((/8|huit/igm).test(tagLance[0])) {
            nbDes = 8;
        } else if((/9|neuf/igm).test(tagLance[0])) {
            nbDes = 9;
        } else {
            nbDes = 1;
        } 

        for (let index = 0; index < nbDes; index++) {
            tabDes[index] = Math.floor(Math.random() * valDes) + 1;
            total = total + tabDes[index];
        }
        
        res.json({
            tab: tabDes,
            total: total,
            nbDes: nbDes,
            valDes: valDes
        })

    } else {
        console.log('Sorry, you are incomprehensible !!!');
        res.json("Sorry, you are incomprehensible !!!");
    }
}