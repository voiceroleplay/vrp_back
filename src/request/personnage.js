var { Personnage } = require('../model/sheetCharacter');
var { Game } = require('../model/game');
var { Account } = require('../model/account');

exports.find = async (request, res) => {
    const personnage = await Personnage.find(request);
    res.json(personnage);
}

exports.save = async (request, res) => {
    const personnage = await Personnage.findOne({_id : request._id});
    if(personnage) {
        await Personnage.updateOne({_id : request._id}, request);
        res.json("Your Personnage has been saved");
    } else {
        
    res.json("Your Personnage has not been saved");
    }
}

exports.create = async (request, res) => {
    const account = await Account.findOne({username : request.username});
    const game = await Game.findOne({name : request.gameName});
    const personnage = await Personnage.findOne({username : request.username, gameName : request.gameName});
    if(!personnage && account && game) {
        await Personnage.create(request);
        res.json("Your Personnage has been created");
    } else if(!account || !game) {
        res.json("Error: This username or this game don't be exist !");
    } else {
        res.json("Error: A character already be exist for this game !");
    }
}

exports.delete = async (request, res) => {
    await Personnage.deleteOne(request);
    res.json("Your Personnage has been deleted");
}
