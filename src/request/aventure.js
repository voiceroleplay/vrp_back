var { Game } = require('../model/game');
var { Story } = require('../model/story');

exports.find = async (request, res) => {
    const aventure = await Game.find(request);
    res.json(aventure);
}

exports.save = async (request, res) => {
    const game = await Game.findOne({_id : request._id});
    if(game) {
        await Game.updateOne({_id : request._id}, request);
        res.json("Your Personnage has been saved");
    } else {
        res.json("Your Personnage has not been saved");
    }
}

exports.create = async (request, res) => {
    const game = await Game.findOne({name : request.name});
    const storie = await Story.findOne({name : request.nameStorie});
    if(!game && storie) {        
        await Game.create(request);
        res.json("Your Aventure has been created");
    } else if (!storie) {
        res.json("Error: This storie don't be exist");
    } else {
        res.json("Error: This name is already used");
    }
}

exports.delete = async (request, res) => {
    await Game.deleteOne(request);
    res.json("Your Aventure has been deleted");
}
