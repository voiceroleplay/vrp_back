var { Feat } = require('../model/feat');

exports.find = async (request, res) => {
    const feat = await Feat.find(request);
    res.json(feat);
}