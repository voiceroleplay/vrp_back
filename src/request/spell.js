var { Spell } = require('../model/spell');

exports.find = async (request, res) => {    
    const spell = await Spell.find(request);
    res.json(spell);
}