var { Classe } = require('../model/classe');

exports.find = async (request, res) => {    
    const classe = await Classe.find(request);
    res.json(classe);
}