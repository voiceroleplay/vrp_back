var { Story } = require('../model/story');

exports.find = async (request, res) => {
    const story = await Story.find(request);
    res.json(story);
}

exports.save = async (request, res) => {
    const story = await Story.findOne({_id : request._id});
    if(story) {
        await Story.updateOne({_id : request._id}, request);
        res.json("Your story has been saved");
    } else {
        res.json("Your story has not been saved");
    }
}

exports.create = async (request, res) => {
    const story = await Story.findOne({name : request.name});
    if(!story) {
        await Story.create(request);
        res.json("Your story has been created");
    } else {
        res.json("Error: This name is already used");
    }
}

exports.delete = async (request, res) => {
    await Story.deleteOne(request);
    res.json("Your story has been deleted");
}