var { Monster } = require('../model/monster');

exports.find = async (request, res) => {    
    const monster = await Monster.find(request);
    res.json(monster);
}