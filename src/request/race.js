var { Race } = require('../model/race');

exports.find = async (request, res) => {
    const race = await Race.find(request);
    res.json(race);
}