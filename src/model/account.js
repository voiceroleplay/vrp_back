var mongoose = require('mongoose');

var accountSchema = new mongoose.Schema({
    lastname: String,
    firstname: String,
    username: String,
});

module.exports.Account = mongoose.model('Account', accountSchema);