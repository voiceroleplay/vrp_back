var mongoose = require('mongoose');
// const { MONGODB_URL } = require('../config/config')
// mongoose.connect(MONGODB_URL, {useNewUrlParser: true, useUnifiedTopology: true});

var featSchema = new mongoose.Schema({
    Id: String,
    Name: String,
    Types: [String],
    Prerequisites: [
        {
          Type: String,
          Number: Number,
          Description: String,
        },
      ],
    Description: String,
    Benefit: String,
    Source: {
      Id: String,
      References: [
        {
          Name: String,
          Href: String,
          HrefString: String,
        }
      ]
    }
  });

module.exports.Feat = mongoose.model('Feat', featSchema);