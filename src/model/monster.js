var mongoose = require('mongoose');
// const { MONGODB_URL } = require('../config/config');
// mongoose.connect(MONGODB_URL, {useNewUrlParser: true, useUnifiedTopology: true});

var monsterSchema = new mongoose.Schema({
    source: {
        _id: String,
    },
    _name: String,
    _cr: String,
    _climate: String,
    _environment: String,
    _type: String,
});

module.exports.Monster = mongoose.model('Monster', monsterSchema);