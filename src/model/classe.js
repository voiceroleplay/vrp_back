var mongoose = require('mongoose');

var classeSchema = new mongoose.Schema({
    Id: String,
    name: String,
    avatar: String,
    des_vie: String,
    argent_depart: String,
    competences: {
        pt_niv: String,
        competences: [{
            Id: String,
            name: String,
            stat: String, 
        }]
    },
    descriptif: [{
        Id: String,
        name: String,
        description: String,
    }]

});

module.exports.Classe = mongoose.model('Classe', classeSchema);