var mongoose = require('mongoose');

var gameSchema = new mongoose.Schema({
    name: String,
    nameStorie: String,
    gm: String,
    players: [],
    room: String,
});

module.exports.Game = mongoose.model('Game', gameSchema);
