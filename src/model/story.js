var mongoose = require('mongoose');
// const { MONGODB_URL } = require('../config/config')
// mongoose.connect(MONGODB_URL, {useNewUrlParser: true, useUnifiedTopology: true});


var storySchema = new mongoose.Schema({
    name: String,
    text: String,
    imgs: [],
    phase: [
        {
            num: Number,
            monsters: [
                {
                    nb: Number, 
                    nameMonster: String,
                }
            ]

        }
    ],
});

module.exports.Story = mongoose.model('Storie', storySchema);