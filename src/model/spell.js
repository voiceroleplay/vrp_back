var mongoose = require('mongoose');
// const { MONGODB_URL } = require('../config/config')
// mongoose.connect(MONGODB_URL, {useNewUrlParser: true, useUnifiedTopology: true});

var spellSchema = new mongoose.Schema({
    Id: String,
    Name: String,
    School: String,
    Levels: [
      {
        List: String,
        Level: Number
      },
    ],
    Components: {
      Kinds: String,
      Description: String,
    },
    Target: {
      Value: String,
    },
    CastingTime: {
      Value: Number,
    },
    Source: {
      Id: String,
      References: [
        {
          Name: String,
          Href: String,
          HrefString: String,
        },
      ]
    },
    Localization: {
      Languages: [
        {
          Entries: [
            {
              Value: String,
              Href: String,
            }
          ],
          Lang: String,
        }
      ]
    }
  });

module.exports.Spell = mongoose.model('Spell', spellSchema);