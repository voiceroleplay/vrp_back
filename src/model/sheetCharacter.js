var mongoose = require('mongoose');

var sheetCharacterSchema = new mongoose.Schema({
    username: String,
    gameName: String,
    sheet: {
        username: String,
        Race: String,
        Sexe: String,
        Age: Number,
        Poids: Number,
        Taille: Number,
        Yeux: String,
        Cheveux: String,
        Peau: String,
        Religion: String,
        Symbole: String,
        Origin: String,
        birthdate: String,
        Alignement: String,
        Dextrie: String,
        stats: {
            For: Number,
            Dex: Number,
            Con: Number,
            Int: Number,
            Sag: Number,
            Cha: Number
            },
        Combat: {
            Init: Number,
            VD: Number,
            Vision: Number,
            BBA: Number,
            BMO: Number,
            DMD: Number,
            CA: Number,
            CA_contact: Number,
            CA_surpris: Number,
            Reflex: Number,
            Vigueur: Number,
            Volonte: Number,
            PDV_actuels: Number,
            PDV_max: Number,
            Dg_non_letaux: Number,
            Armes_et_mode: [
                {
                    name: String,
                    dmg: String | Number,
                    mode: String,
                    distance: Number,
                }],
            },
            Apt_Cap_Trt_Don_Lng: {
                Classe: [],
                Race: [],
                Trait: [],
                Competences_martiales: [],
                Dons: [],
                Langues: [],
            },
            Competences: [
                {
                    name: String,
                    val: Number
                }
            ],
            Equipement: [],
            Magie: [
                {
                    name: String,
                    dmg: String,
                    effect: String
                }
            ]
    }
});

module.exports.Personnage = mongoose.model('SheetCharacter', sheetCharacterSchema);
