var mongoose = require('mongoose');

var raceSchema = new mongoose.Schema({
    Id: String,
    name: String,
    avatar: String,
    description_physique: String,
    société: String,
    relations: String,
    alignement_religion: String,
    aventuriers: String,
    traits_raciaux: [[{
        Id: String,
        name: String,
        description: String,
    }]],
    bonus_classe: [{
        Id: String,
        name: String,
        description: String,
    }]

});

module.exports.Race = mongoose.model('Race', raceSchema);