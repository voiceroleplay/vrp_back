var chai = require ('chai');
var chaiHttp = require ('chai-http');
require('dotenv').config();
var app = require ('../app');
var rp = require('request-promise')

// Configure chai
chai.use(chaiHttp);
chai.should();

var admin = require("firebase-admin");

const uid = 'test-uid'

describe("Index", () => {
  describe("GET /", () => {
    before(async () => {
      try {
        customToken = await admin.auth().createCustomToken(uid);
        const res = await rp({
          url: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyCustomToken?key=AIzaSyDGtuzBKaNR2ouVRhdW1Nc7PW56jOoK32g',
          method: 'POST',
          body: {
              token: customToken,
              returnSecureToken: true,
          },
          json: true
        });

        idToken = res.idToken;
      } catch (error) {

      }
    });

    it("should return status 200", (done) => {
      chai.request(app)
        .get('/')
        .set('authToken', idToken)
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });

    it("should return status 200", (done) => {
      chai.request(app)
        .get('/authToken')
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });

    it("should return Unauthorized because no token", (done) => {
      chai.request(app)
        .get(`/`)
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });

    it("should return Unauthorized because bad toekn", (done) => {
      chai.request(app)
        .get(`/`)
        .set('authToken', 'error')
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });
      
    it("should not find the page", (done) => {
      chai.request(app)
        .get(`/error`)
        .set('authToken', idToken)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });

    it("should get error 500", (done) => {
      chai.request(app)
        .get(`/debug`)
        .set('authToken', idToken)
        .end((err, res) => {
          res.should.have.status(500);
          done();
        });
    });
  });
});