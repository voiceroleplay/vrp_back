var chai = require ('chai');
var spies = require('chai-spies');
var chaiHttp = require ('chai-http');
var sinon = require('sinon');
require('dotenv').config();
var app = require ('../../app');
var rp = require('request-promise')
var { Story } = require('../../model/story');

// Configure chai
chai.use(spies);
chai.use(chaiHttp);
chai.should();

var admin = require("firebase-admin");
const uid = 'test-uid'

describe("Story route", () => {
  describe("GET /story", () => {
    before(async () => {
      try {
        customToken = await admin.auth().createCustomToken(uid);
        const res = await rp({
          url: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyCustomToken?key=AIzaSyDGtuzBKaNR2ouVRhdW1Nc7PW56jOoK32g',
          method: 'POST',
          body: {
              token: customToken,
              returnSecureToken: true,
          },
          json: true
        });

        idToken = res.idToken;
      } catch (error) {
      }
    });
    afterEach(function () {
      sinon.restore();
    });

    it("should call Story.find", (done) => {    
        var mock = sinon.stub(Story, 'find').returns(true);
        chai.request(app)
            .get('/story')
            .set('authToken', idToken)
            .end((err, res) => {
                sinon.assert.called(mock);
                done();
            });
    });

    it("should call Story.create", (done) => {    
        var mock = sinon.stub(Story, 'findOne').returns(null);
        var mock1 = sinon.stub(Story, 'create').returns(true);
        chai.request(app)
            .post('/story')
            .set('authToken', idToken)
            .send({
                "name": "String",
                "text": "String",
                imgs: [],
                phase: [{"num": 8,monsters: [{"nb": 5, "nameMonster": "String",}]}],
            })
            .end((err, res) => {
                sinon.assert.called(mock);
                sinon.assert.called(mock1);
                done();
            });
    });

    it("should not call Story.create because story already exist", (done) => {    
      var mock = sinon.stub(Story, 'findOne').returns(true);
      var mock1 = sinon.stub(Story, 'create').returns(true);
      chai.request(app)
          .post('/story')
          .set('authToken', idToken)
          .send({
              "name": "String",
              "text": "String",
              imgs: [],
              phase: [{"num": 8,monsters: [{"nb": 5, "nameMonster": "String",}]}],
          })
          .end((err, res) => {
              sinon.assert.called(mock);
              sinon.assert.notCalled(mock1);
              done();
          });
    });

    it("should call Story.updateOne", (done) => {    
      var mock = sinon.stub(Story, 'findOne').returns(true);
      var mock1 = sinon.stub(Story, 'updateOne').returns(true);
      chai.request(app)
          .put('/story')
          .set('authToken', idToken)
          .send({
              "name": "String",
              "text": "String",
              imgs: [],
              phase: [{"num": 8,monsters: [{"nb": 5, "nameMonster": "String",}]}],
          })
          .end((err, res) => {
              sinon.assert.called(mock);
              sinon.assert.called(mock1);
              done();
          });
    });

    it("should not call Story.updateOne because Story does not exist", (done) => {    
      var mock = sinon.stub(Story, 'findOne').returns(null);
      var mock1 = sinon.stub(Story, 'updateOne').returns(true);
      chai.request(app)
          .put('/story')
          .set('authToken', idToken)
          .send({
              "name": "String",
              "text": "String",
              imgs: [],
              phase: [{"num": 8,monsters: [{"nb": 5, "nameMonster": "String",}]}],
          })
          .end((err, res) => {
              sinon.assert.called(mock);
              sinon.assert.notCalled(mock1);
              done();
          });
    });

    it("should return Unauthorized because no token", (done) => {
      chai.request(app)
        .get(`/story`)
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });
  });
});