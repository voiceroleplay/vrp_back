var chai = require ('chai');
var spies = require('chai-spies');
var chaiHttp = require ('chai-http');
var sinon = require('sinon');
require('dotenv').config();
var app = require ('../../app');
var rp = require('request-promise')
var { Personnage } = require('../../model/sheetCharacter');
var { Game } = require('../../model/game');
var { Account } = require('../../model/account');

// Configure chai
chai.use(spies);
chai.use(chaiHttp);
chai.should();

var admin = require("firebase-admin");
const uid = 'test-uid'

describe("Personnage route", () => {
  describe("GET /personnage", () => {
    before(async () => {
      try {
        customToken = await admin.auth().createCustomToken(uid);
        const res = await rp({
          url: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyCustomToken?key=AIzaSyDGtuzBKaNR2ouVRhdW1Nc7PW56jOoK32g',
          method: 'POST',
          body: {
              token: customToken,
              returnSecureToken: true,
          },
          json: true
        });

        idToken = res.idToken;
      } catch (error) {
      }
    });
    afterEach(function () {
      sinon.restore();
    });

    it("should call Personnage.find", (done) => {    
        var mock = sinon.stub(Personnage, 'find').returns(true);
        chai.request(app)
            .get('/personnage')
            .set('authToken', idToken)
            .end((err, res) => {
                sinon.assert.called(mock);
                done();
            });
    });

    it("should call Personnage.create", (done) => {    
        var mock = sinon.stub(Personnage, 'create').returns(true);
        var mock1 = sinon.stub(Personnage, 'findOne').returns(null);
        var mock2 = sinon.stub(Game, 'findOne').returns(true);
        var mock3 = sinon.stub(Account, 'findOne').returns(true);
        chai.request(app)
            .post('/personnage')
            .set('authToken', idToken)
            .send({
              "username": "String",
              "gameName": "String",
              "sheet": {},
            }).end((res) => {
              sinon.assert.called(mock);
              sinon.assert.called(mock1);
              sinon.assert.called(mock2);
              sinon.assert.called(mock3);
              done();
           });
    });

    it("should not call Personnage.create because personnage already exist", (done) => {    
      var mock = sinon.stub(Personnage, 'create').returns(true);
      var mock1 = sinon.stub(Personnage, 'findOne').returns(true);
      var mock2 = sinon.stub(Game, 'findOne').returns(true);
      var mock3 = sinon.stub(Account, 'findOne').returns(true);
      chai.request(app)
          .post('/personnage')
          .set('authToken', idToken)
          .send({
            "username": "String",
            "gameName": "String",
            "sheet": {},
          }).end((res) => {
            sinon.assert.notCalled(mock);
            sinon.assert.called(mock1);
            sinon.assert.called(mock2);
            sinon.assert.called(mock3);
            done();
         });
  });

  it("should not call Personnage.create because no Game", (done) => {    
    var mock = sinon.stub(Personnage, 'create').returns(true);
    var mock1 = sinon.stub(Personnage, 'findOne').returns(null);
    var mock2 = sinon.stub(Game, 'findOne').returns(null);
    var mock3 = sinon.stub(Account, 'findOne').returns(true);
    chai.request(app)
        .post('/personnage')
        .set('authToken', idToken)
        .send({
          "username": "String",
          "gameName": "String",
          "sheet": {},
        }).end((res) => {
          sinon.assert.notCalled(mock);
          sinon.assert.called(mock1);
          sinon.assert.called(mock2);
          sinon.assert.called(mock3);
          done();
       });
  });

  it("should not call Personnage.create because no Account", (done) => {    
    var mock = sinon.stub(Personnage, 'create').returns(true);
    var mock1 = sinon.stub(Personnage, 'findOne').returns(null);
    var mock2 = sinon.stub(Game, 'findOne').returns(true);
    var mock3 = sinon.stub(Account, 'findOne').returns(null);
    chai.request(app)
        .post('/personnage')
        .set('authToken', idToken)
        .send({
          "username": "String",
          "gameName": "String",
          "sheet": {},
        }).end((res) => {
          sinon.assert.notCalled(mock);
          sinon.assert.called(mock1);
          sinon.assert.called(mock2);
          sinon.assert.called(mock3);
          done();
       });
  });

  it("should not call Personnage.create because no Account nor Game", (done) => {    
    var mock = sinon.stub(Personnage, 'create').returns(true);
    var mock1 = sinon.stub(Personnage, 'findOne').returns(null);
    var mock2 = sinon.stub(Game, 'findOne').returns(null);
    var mock3 = sinon.stub(Account, 'findOne').returns(null);
    chai.request(app)
        .post('/personnage')
        .set('authToken', idToken)
        .send({
          "username": "String",
          "gameName": "String",
          "sheet": {},
        }).end((res) => {
          sinon.assert.notCalled(mock);
          sinon.assert.called(mock1);
          sinon.assert.called(mock2);
          sinon.assert.called(mock3);
          done();
       });
  });

  it("should call Personnage.updateOne", (done) => {    
    var mock = sinon.stub(Personnage, 'updateOne').returns(true);
    var mock1 = sinon.stub(Personnage, 'findOne').returns(true);
    chai.request(app)
        .put('/personnage')
        .set('authToken', idToken)
        .send({
          "username": "String",
          "gameName": "String",
          "sheet": {},
        }).end((res) => {
          sinon.assert.called(mock);
          sinon.assert.called(mock1);
          done();
       });
  });

  it("should not call Personnage.updateOne because personnage does not exist", (done) => {    
    var mock = sinon.stub(Personnage, 'updateOne').returns(true);
    var mock1 = sinon.stub(Personnage, 'findOne').returns(false);
    chai.request(app)
        .put('/personnage')
        .set('authToken', idToken)
        .send({
          "username": "String",
          "gameName": "String",
          "sheet": {},
        }).end((res) => {
          sinon.assert.notCalled(mock);
          sinon.assert.called(mock1);
          done();
       });
  });

  it("should call Personnage.deleteOne", (done) => {    
    var mock = sinon.stub(Personnage, 'deleteOne').returns(true);
    chai.request(app)
        .delete('/personnage?name=test')
        .set('authToken', idToken).end((res) => {
          sinon.assert.called(mock);
          done();
       });
  });

    it("should return Unauthorized because no token", (done) => {
      chai.request(app)
        .get(`/personnage`)
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });
  });
});