var chai = require ('chai');
var spies = require('chai-spies');
var chaiHttp = require ('chai-http');
var sinon = require('sinon');
require('dotenv').config();
var app = require ('../../app');
var rp = require('request-promise')
var { Account } = require('../../model/account');

// Configure chai
chai.use(spies);
chai.use(chaiHttp);
chai.should();

var admin = require("firebase-admin");
const uid = 'test-uid'

describe("Account route", () => {
  describe("GET /account", () => {
    before(async () => {
      try {
        customToken = await admin.auth().createCustomToken(uid);
        const res = await rp({
          url: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyCustomToken?key=AIzaSyDGtuzBKaNR2ouVRhdW1Nc7PW56jOoK32g',
          method: 'POST',
          body: {
              token: customToken,
              returnSecureToken: true,
          },
          json: true
        });

        idToken = res.idToken;
      } catch (error) {
      }
    });
    afterEach(function () {
      sinon.restore();
    });

    it("should call Account.find", (done) => {    
        var mock = sinon.stub(Account, 'find').returns(true);
        chai.request(app)
            .get('/account')
            .set('authToken', idToken)
            .end((err, res) => {
                sinon.assert.called(mock);
                done();
            });
    });

    it("should call Account.create", (done) => {    
        var mock = sinon.stub(Account, 'create').returns(true)
        var mock1 = sinon.stub(Account, 'findOne').returns(null);
        chai.request(app)
            .post('/account')
            .set('authToken', idToken)
            .send({
                "lastname": "lastname",
                "firstname": "firstname",
                "username": "username",
            })
            .end((err, res) => {
                sinon.assert.called(mock);
                sinon.assert.called(mock1);
                done();
            });
    });

    it("should not call Account.create because Account already exist", (done) => {    
      var mock = sinon.stub(Account, 'create').returns(true)
      var mock1 = sinon.stub(Account, 'findOne').returns(true);
      chai.request(app)
          .post('/account')
          .set('authToken', idToken)
          .send({
              "lastname": "lastname",
              "firstname": "firstname",
              "username": "username",
          })
          .end((err, res) => {
              sinon.assert.notCalled(mock);
              sinon.assert.called(mock1);
              done();
          });
  });

      it("should call Account.updateOne", (done) => {    
        var mock = sinon.stub(Account, 'updateOne').returns(true)
        var mock1 = sinon.stub(Account, 'findOne').returns(true);
        chai.request(app)
            .put('/account')
            .set('authToken', idToken)
            .send({
                "lastname": "lastname",
                "firstname": "firstname",
                "username": "username",
            })
            .end((err, res) => {
                sinon.assert.called(mock);
                sinon.assert.called(mock1);
                done();
            });
    });

    it("should not call Account.updateOne because Account does not exist", (done) => {    
      var mock = sinon.stub(Account, 'updateOne').returns(true)
      var mock1 = sinon.stub(Account, 'findOne').returns(null);
      chai.request(app)
          .put('/account')
          .set('authToken', idToken)
          .send({
              "lastname": "lastname",
              "firstname": "firstname",
              "username": "username",
          })
          .end((err, res) => {
              sinon.assert.notCalled(mock);
              sinon.assert.called(mock1);
              done();
          });
    });

    it("should return Unauthorized because no token", (done) => {
      chai.request(app)
        .get(`/account`)
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });
  });
});