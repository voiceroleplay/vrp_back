var chai = require ('chai');
var spies = require('chai-spies');
var chaiHttp = require ('chai-http');
var sinon = require('sinon');
require('dotenv').config();
var app = require ('../../app');
var rp = require('request-promise');
const expect = chai.expect;

// Configure chai
chai.use(spies);
chai.use(chaiHttp);
chai.should();

var admin = require("firebase-admin");
const uid = 'test-uid'

describe("vocalReconize route", () => {
  describe("GET /vocalReconize", () => {
    
    it("should call vocalReconize.find", (done) => {    
        chai.request(app)
            .get('/vocalReconize')
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });

    it('lance 1 dé', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 1 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(1)
                done();
            });
    });

    it('lance 2 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 2 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(2)
                done();
            });
    });

    it('lance 3 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 3 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(3)
                done();
            });
    });

    it('lance 4 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 4 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(4)
                done();
            });
    });

    it('lance 5 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 5 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(5)
                done();
            });
    });

    it('lance 6 dé', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 6 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(6)
                done();
            });
    });

    it('lance 7 dé', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 7 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(7)
                done();
            });
    });

    it('lance 8 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 8 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(8)
                done();
            });
    });

    it('lance 9 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 9 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(9)
                done();
            });
    });

    it('lance 10 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 10 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(10)
                done();
            });
    });

    it('lance 11 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 11 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(11)
                done();
            });
    });

    it('lance 12 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 12 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(12)
                done();
            });
    });

    it('lance 13 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 13 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(13)
                done();
            });
    });

    it('lance 14 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 14 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(14)
                done();
            });
    });

    it('lance 15 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 15 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(15)
                done();
            });
    });

    it('lance 16 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 16 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(16)
                done();
            });
    });

    it('lance 17 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance dix-sept des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(17)
                done();
            });
    });

    it('lance 18 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 18 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(18)
                done();
            });
    });

    it('lance 19 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 19 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(19)
                done();
            });
    });

    it('lance 20 dés', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 20 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property("tab").with.lengthOf(20)
                done();
            });
    });

    it('Dé 4', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 1 des 4'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body.valDes).to.equal(4)
                done();
            });
    });

    it('Dé 6', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 1 des 6'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body.valDes).to.equal(6)
                done();
            });
    });

    it('Dé 8', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 1 des 8'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body.valDes).to.equal(8)
                done();
            });
    });

    it('Dé 10', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 1 des 10'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body.valDes).to.equal(10)
                done();
            });
    });

    it('Dé 12', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 1 des 12'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body.valDes).to.equal(12)
                done();
            });
    });

    it('Dé 20', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 1 des 20'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body.valDes).to.equal(20)
                done();
            });
    });

    it('Dé 100', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'lance 1 des 100'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body.valDes).to.equal(100)
                done();
            });
    });

    it('incomprehensible', (done) =>  {
      chai.request(app)
            .get('/vocalReconize')
            .query({val: 'Bonjour'})
            .set('authToken', idToken)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.equal('Sorry, you are incomprehensible !!!')
                done();
            });
    });

    it("should return Unauthorized because no token", (done) => {
      chai.request(app)
        .get(`/vocalReconize`)
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });
  });
});