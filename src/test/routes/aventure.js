var chai = require ('chai');
var spies = require('chai-spies');
var chaiHttp = require ('chai-http');
var sinon = require('sinon');
require('dotenv').config();
var app = require ('../../app');
var rp = require('request-promise')
var { Game } = require('../../model/game');
var { Story } = require('../../model/story');

// Configure chai
chai.use(spies);
chai.use(chaiHttp);
chai.should();

var admin = require("firebase-admin");
const uid = 'test-uid'

describe("Aventure route", () => {
  describe("GET /aventure", () => {
    before(async () => {
      try {
        customToken = await admin.auth().createCustomToken(uid);
        const res = await rp({
          url: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyCustomToken?key=AIzaSyDGtuzBKaNR2ouVRhdW1Nc7PW56jOoK32g',
          method: 'POST',
          body: {
              token: customToken,
              returnSecureToken: true,
          },
          json: true
        });

        idToken = res.idToken;
      } catch (error) {
      }
    });
    afterEach(function () {
      sinon.restore();
    });

    it("should call Game.find", (done) => {    
      var mock = sinon.stub(Game, 'find').returns(true);
      chai.request(app)
          .get('/aventure')
          .set('authToken', idToken)
          .end((err, res) => {
              sinon.assert.called(mock);
              done();
          });
  });

    it("should call Game.create", (done) => {    
        var mock = sinon.stub(Game, 'create').returns(true);
        var mock1 = sinon.stub(Game, 'findOne').returns(null);
        var mock2 = sinon.stub(Story, 'findOne').returns(true);
        chai.request(app)
            .post('/aventure')
            .set('authToken', idToken)
            .send({
                "name": "String",
                "nameStorie": "String",
                "gm": "String",
                players: ["Artile"],
                "room": "String",
            })
            .end((err, res) => {
                sinon.assert.called(mock);
                sinon.assert.called(mock1);
                sinon.assert.called(mock2);
                done();
            });
    });

    it("should not call Game.create because game already exist", (done) => {    
      var mock = sinon.stub(Game, 'create').returns(true);
      var mock1 = sinon.stub(Game, 'findOne').returns(true);
      var mock2 = sinon.stub(Story, 'findOne').returns(true);
      chai.request(app)
          .post('/aventure')
          .set('authToken', idToken)
          .send({
              "name": "String",
              "nameStorie": "String",
              "gm": "String",
              players: ["Artile"],
              "room": "String",
          })
          .end((err, res) => {
              sinon.assert.notCalled(mock);
              sinon.assert.called(mock1);
              sinon.assert.called(mock2);
              done();
          });
    });

    it("should not call Game.create because no Story", (done) => {    
      var mock = sinon.stub(Game, 'create').returns(true);
      var mock1 = sinon.stub(Game, 'findOne').returns(null);
      var mock2 = sinon.stub(Story, 'findOne').returns(null);
      chai.request(app)
          .post('/aventure')
          .set('authToken', idToken)
          .send({
              "name": "String",
              "nameStorie": "String",
              "gm": "String",
              players: ["Artile"],
              "room": "String",
          })
          .end((err, res) => {
              sinon.assert.notCalled(mock);
              sinon.assert.called(mock1);
              sinon.assert.called(mock2);
              done();
          });
    });

    it("should call Game.updateOne", (done) => {    
      var mock = sinon.stub(Game, 'updateOne').returns(true);
      var mock1 = sinon.stub(Game, 'findOne').returns(true);
      chai.request(app)
          .put('/aventure')
          .set('authToken', idToken)
          .send({
              "name": "String",
              "nameStorie": "String",
              "gm": "String",
              players: ["Artile"],
              "room": "String",
          })
          .end((err, res) => {
              sinon.assert.called(mock);
              sinon.assert.called(mock1);
              done();
          });
    });

    it("should not call Game.updateOne Game does not exist", (done) => {    
      var mock = sinon.stub(Game, 'updateOne').returns(true);
      var mock1 = sinon.stub(Game, 'findOne').returns(null);
      chai.request(app)
          .put('/aventure')
          .set('authToken', idToken)
          .send({
              "name": "String",
              "nameStorie": "String",
              "gm": "String",
              players: ["Artile"],
              "room": "String",
          })
          .end((err, res) => {
              sinon.assert.notCalled(mock);
              sinon.assert.called(mock1);
              done();
          });
    });

    

  it("should call Aventure.deleteOne", (done) => {    
    var mock = sinon.stub(Game, 'deleteOne').returns(true);
    chai.request(app)
        .delete('/aventure?name=test')
        .set('authToken', idToken).end((res) => {
          sinon.assert.called(mock);
          done();
       });
  });

    it("should return Unauthorized because no token", (done) => {
      chai.request(app)
        .get(`/aventure`)
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });
  });
});