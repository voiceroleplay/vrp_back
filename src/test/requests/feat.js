"use strict";
const mongoose = require('mongoose');
const chai = require('chai');
const expect = chai.expect;
var { Feat } = require('../../model/feat');

describe('Test feat', ()  =>  {
  before((done) => {
    mongoose.connect('mongodb://vps1.webcoastagency.com:27017/VoiceRolePlay', {useNewUrlParser: true, useUnifiedTopology: true});
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', () => {
      done();
    });
  });

  describe('Test feat', () =>  {
    it('New name saved to test database', (done) =>  {
      var testfeat = Feat({
        Id: "test",
        Name: "test",
        Types: ["test"],
        Prerequisites: [
            {
              Type: "test",
              Number: 1,
              Description: "test",
            },
          ],
        Description: "test",
        Benefit: "test",
        Source: {
          Id: "test",
          References: [
            {
              Name: "test",
              Href: "test",
              HrefString: "test",
            }
          ]
        }
      });
 
        testfeat.save(done);
    });

    it('Should retrieve from test database', (done) =>  {
        Feat.findOne({Name: 'test'}, async (err, feat) => {
        if(err) {throw err;}
        if(feat.length === 0) {throw new Error('No data!');}
        await Feat.deleteOne({Name: 'test'});
        done();
      });
    });
  });


  after((done) => {
    mongoose.connection.close(done);
  });
});