"use strict";
const mongoose = require('mongoose');
const chai = require('chai');
const expect = chai.expect;
var { Spell } = require('../../model/spell');

describe('Test spell', ()  =>  {
  before((done) => {
    mongoose.connect('mongodb://vps1.webcoastagency.com:27017/VoiceRolePlay', {useNewUrlParser: true, useUnifiedTopology: true});
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', () => {
      done();
    });
  });

  describe('Test spell', () =>  {
    it('New name saved to test database', (done) =>  {
      var testspell = Spell({
        Id: "Test",
        Name: "Test",
        School: "Test",
        Levels: [
          {
            List: "Test",
            Level: 1
          },
        ],
        Components: {
          Kinds: "Test",
          Description: "Test",
        },
        Target: {
          Value: "Test",
        },
        CastingTime: {
          Value: 1,
        },
        Source: {
          Id: "Test",
          References: [
            {
              Name: "Test",
              Href: "Test",
              HrefString: "Test",
            },
          ]
        },
        Localization: {
          Languages: [
            {
              Entries: [
                {
                  Value: "Test",
                  Href: "Test",
                }
              ],
              Lang: "Test",
            }
          ]
        }
      });
 
        testspell.save(done);
    });

    it('Should retrieve from test database', (done) =>  {
        Spell.findOne({ Name: 'Test' }, async (err, spell) => {
        if(err) {throw err;}
        if(spell.length === 0) {throw new Error('No data!');}
          const res = await Spell.deleteOne({ Name: 'Test' });
        done();
      });
    });
  });


  after((done) => {
    mongoose.connection.close(done);
  });
});