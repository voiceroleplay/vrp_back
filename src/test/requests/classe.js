"use strict";
const mongoose = require('mongoose');
const chai = require('chai');
const expect = chai.expect;
var { Classe } = require('../../model/classe');

describe('Test Classe', ()  =>  {
  before((done) => {
    mongoose.connect('mongodb://vps1.webcoastagency.com:27017/VoiceRolePlay', {useNewUrlParser: true, useUnifiedTopology: true});
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', () => {
      done();
    });
  });

  describe('Test Classe', () =>  {
    it('New name saved to test database', (done) =>  {
      var testClasse = Classe({
        Id: "1",
        name: "Test",
        avatar: "Test",
        des_vie: "Test",
        argent_depart: "Test",
        competences: {
            pt_niv: "Test",
            competences: [{
                Id: "Test",
                name: "Test",
                stat: "Test", 
            }]
        },
        descriptif: [{
            Id: "Test",
            name: "Test",
            description: "Test",
        }]
    
    });
 
        testClasse.save(done);
    });

    it('Should retrieve from test database', (done) =>  {
      Classe.findOne({name: 'Test'}, async (err, classe) => {
        if(err) {throw err;}
        if(classe.length === 0) {throw new Error('No data!');}
        await Classe.deleteOne({name: 'Test'});
        done();
      });
    });
  });


  after((done) => {
    mongoose.connection.close(done);
  });
});