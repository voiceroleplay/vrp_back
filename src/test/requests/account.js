"use strict";
const mongoose = require('mongoose');
const chai = require('chai');
const expect = chai.expect;
var { Account } = require('../../model/account');

describe('Database Tests', ()  =>  {
  before((done) => {
    mongoose.connect('mongodb://vps1.webcoastagency.com:27017/VoiceRolePlay', {useNewUrlParser: true, useUnifiedTopology: true});
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', () => {
      done();
    });
  });

  describe('Test saving', () =>  {
    it('New name saved to test database', (done) =>  {
      var testAccount = Account({
        lastname: "Test",
        firstname: "Test",
        username: "Test",
    });
 
        testAccount.save(done);
    });

    it('Should retrieve from test database', (done) =>  {
      Account.findOne({lastname: 'Test'}, (err, lastname) => {
        if(err) {throw err;}
        if(lastname.length === 0) {throw new Error('No data!');}
        done();
      });
    });

    it('Should modify data from test database', (done) =>  {
      Account.find({lastname: 'Test'}, (err, account) => {
      account[0].lastname = "TestModif"
      Account.updateOne({_id: account[0]._id}, account[0], (err, account) => {
        if(err) {throw err;}
        if(account.length === 0) {throw new Error('No data!');}
        done();
      });
      });
      
    });

    it('Should delete data from test database', (done) =>  {
      Account.deleteOne({lastname: 'TestModif'}, (err, lastname) => {
        if(err) {throw err;}
        if(lastname.length === 0) {throw new Error('No data!');}
        done();
      });
    });
  });


  after(function(done){
    mongoose.connection.close(done);
  });
});