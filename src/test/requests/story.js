"use strict";
const mongoose = require('mongoose');
const chai = require('chai');
const expect = chai.expect;
var { Story } = require('../../model/story');

describe('Database Tests', ()  =>  {
  before((done) => {
    mongoose.connect('mongodb://vps1.webcoastagency.com:27017/VoiceRolePlay', {useNewUrlParser: true, useUnifiedTopology: true});
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', () => {
      done();
    });
  });

  describe('Test Database', () =>  {
    it('New name saved to test database', (done) =>  {
      var teststory = Story({
        name: "Test",
        text: "Test",
        imgs: [],
        phase: [
            {
                num: 1,
                monsters: [
                    {
                        nb: 1, 
                        nameMonster: "Test",
                    }
                ]
    
            }
        ],
    });
 
        teststory.save(done);
    });

    it('Should retrieve from test database', (done) =>  {
        Story.findOne({name: 'Test'}, (err, story) => {
        if(err) {throw err;}
        if(story.length === 0) {throw new Error('No data!');}
        done();
      });
    });

    it('Should modify data from test database', (done) =>  {
        Story.find({name: 'Test'}, (err, story) => {
      story[0].name = "TestModif"
      Story.updateOne({_id: story[0]._id}, story[0], (err, story) => {
        if(err) {throw err;}
        if(story.length === 0) {throw new Error('No data!');}
        done();
      });
      });
      
    });

    it('Should delete data from test database', (done) =>  {
        Story.deleteOne({name: 'TestModif'}, (err, story) => {
        if(err) {throw err;}
        if(story.length === 0) {throw new Error('No data!');}
        done();
      });
    });
  });


  after(function(done){
    mongoose.connection.close(done);
  });
});