"use strict";
const mongoose = require('mongoose');
const chai = require('chai');
const expect = chai.expect;
var { Personnage } = require('../../model/sheetCharacter');

describe('Database Tests', ()  =>  {
  before((done) => {
    mongoose.connect('mongodb://vps1.webcoastagency.com:27017/VoiceRolePlay', {useNewUrlParser: true, useUnifiedTopology: true});
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', () => {
      done();
    });
  });

  describe('Test Database', () =>  {
    it('New name saved to test database', (done) =>  {
      var testpersonnage = Personnage({
        username: "Test",
        gameName: "Test",
        sheet: {},
    });
 
        testpersonnage.save(done);
    });

    it('Should retrieve from test database', (done) =>  {
        Personnage.findOne({username: 'Test'}, (err, personnage) => {
        if(err) {throw err;}
        if(personnage.length === 0) {throw new Error('No data!');}
        done();
      });
    });

    it('Should modify data from test database', (done) =>  {
        Personnage.find({username: 'Test'}, (err, personnage) => {
      personnage[0].username = "TestModif"
      Personnage.updateOne({_id: personnage[0]._id}, personnage[0], (err, personnage) => {
        if(err) {throw err;}
        if(personnage.length === 0) {throw new Error('No data!');}
        done();
      });
      });
      
    });

    it('Should delete data from test database', (done) =>  {
        Personnage.deleteOne({username: 'TestModif'}, (err, personnage) => {
        if(err) {throw err;}
        if(personnage.length === 0) {throw new Error('No data!');}
        done();
      });
    });
  });


  after(function(done){
    mongoose.connection.close(done);
  });
});