"use strict";
const mongoose = require('mongoose');
const chai = require('chai');
const expect = chai.expect;
var { Game } = require('../../model/game');

describe('Database Tests', ()  =>  {
  before((done) => {
    mongoose.connect('mongodb://vps1.webcoastagency.com:27017/VoiceRolePlay', {useNewUrlParser: true, useUnifiedTopology: true});
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', () => {
      done();
    });
  });

  describe('Test Database', () =>  {
    it('New name saved to test database', (done) =>  {
      var testAventure = Game({
        name: "Test",
        nameStorie: "Test",
        gm: "Test",
        players: [],
        room: "Test",
    });
 
        testAventure.save(done);
    });

    it('Should retrieve from test database', (done) =>  {
        Game.findOne({name: 'Test'}, (err, aventure) => {
        if(err) {throw err;}
        if(aventure.length === 0) {throw new Error('No data!');}
        done();
      });
    });

    it('Should modify data from test database', (done) =>  {
        Game.find({name: 'Test'}, (err, aventure) => {
      aventure[0].name = "TestModif"
      Game.updateOne({_id: aventure[0]._id}, aventure[0], (err, aventure) => {
        if(err) {throw err;}
        if(aventure.length === 0) {throw new Error('No data!');}
        done();
      });
      });
      
    });

    it('Should delete data from test database', (done) =>  {
        Game.deleteOne({name: 'TestModif'}, (err, aventure) => {
        if(err) {throw err;}
        if(aventure.length === 0) {throw new Error('No data!');}
        done();
      });
    });
  });


  after(function(done){
    mongoose.connection.close(done);
  });
});