"use strict";
const mongoose = require('mongoose');
const chai = require('chai');
const expect = chai.expect;
var { Race } = require('../../model/race');

describe('Test race', ()  =>  {
  before((done) => {
    mongoose.connect('mongodb://vps1.webcoastagency.com:27017/VoiceRolePlay', {useNewUrlParser: true, useUnifiedTopology: true});
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', () => {
      done();
    });
  });

  describe('Test race', () =>  {
    it('New name saved to test database', (done) =>  {
      var testrace = Race({
        Id: "Test",
        name: "Test",
        avatar: "Test",
        description_physique: "Test",
        société: "Test",
        relations: "Test",
        alignement_religion: "Test",
        aventuriers: "Test",
        traits_raciaux: [[{
            Id: "Test",
            name: "Test",
            description: "Test",
        }]],
        bonus_classe: [{
            Id: "Test",
            name: "Test",
            description: "Test",
        }]
    
    });
 
        testrace.save(done);
    });

    it('Should retrieve from test database', (done) =>  {
        Race.findOne({name: 'Test'}, async (err, race) => {
        if(err) {throw err;}
        if(race.length === 0) {throw new Error('No data!');}
        await Race.deleteOne({Id: 'Test'});
        done();
      });
    });
  });


  after((done) => {
    mongoose.connection.close(done);
  });
});